package hust.soict.globalict.lab01;

import javax.swing.JOptionPane;

// Example 2: FirstDialog.java
public class FirstDialog {
    public static void main(String[] args) {
        JOptionPane.showMessageDialog(null, "Hello world! How are you?");
        System.exit(0);
    }
}
