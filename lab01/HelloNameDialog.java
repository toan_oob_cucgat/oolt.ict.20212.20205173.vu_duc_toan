package hust.soict.globalict.lab01;

import javax.swing.JOptionPane;

// Example 3: HelloNameDialog.java
public class HelloNameDialog {
    public static void main(String[] args) {
        String result;
        result = JOptionPane.showInputDialog("Please enter your name:");
        JOptionPane.showMessageDialog(null, " Hi " + result + "!");
        System.exit(0);
    }
}
