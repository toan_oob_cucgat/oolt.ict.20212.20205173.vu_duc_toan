package hust.soict.globalict.lab02;

import java.util.Arrays;
import java.util.Scanner;

public class JavaArrays {
    public static void main(String[] args) {
        Scanner reader = new Scanner(System.in);

        System.out.print("n: ");
        int n = reader.nextInt();

        int[] a = new int[n];
        for (int i = 0; i < n; i++) {
            System.out.print("a[" + i + "]: ");
            a[i] = reader.nextInt();
        }

        System.out.print("Array: ");
        System.out.println(Arrays.toString(a));

        Arrays.sort(a);
        System.out.print("Sorted: ");
        System.out.println(Arrays.toString(a));


        int sum = 0;
        for (int i = 0; i < n; i++) {
            sum += a[i];
        }

        System.out.println("Sum: " + sum);

        if (a.length == 0) {
            System.out.println("Cannot divide by zero!");
        } else {
            System.out.println("Average: " + 1.0 * sum / a.length);
        }
    }
}
